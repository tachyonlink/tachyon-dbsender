-- Tachyon DB Sender
-- Copyright (C) 2014 Michal Belica <devel@beli.sk>
--
-- This file is part of Tachyon DB Sender.
--
-- Tachyon DB Sender is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Tachyon DB Sender is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

-- Example of creating notification triggers.

-- Function to fire notification "new_event" when called from a trigger.
CREATE FUNCTION notify_new_event() RETURNS trigger AS $$
DECLARE
BEGIN
	  NOTIFY new_event;
	  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

-- Trigger on table "events" which calls function "notify_new_event()" on
-- INSERTs, once after for each statement.
CREATE TRIGGER new_event_trigger AFTER INSERT ON events
FOR EACH STATEMENT EXECUTE PROCEDURE notify_new_event();

