#!/usr/bin/env python

# Tachyon DB Sender
# Copyright (C) 2014 Michal Belica <devel@beli.sk>
#
# This file is part of Tachyon DB Sender.
#
# Tachyon DB Sender is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tachyon DB Sender is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tachyon DB Sender.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# get version number
version = {}
with open("src/tachyon_dbsender/version.py") as f:
    exec(f.read(), version)

setup(
    name='tachyon-dbsender',
    version=version['__version__'],
    description='Tachyon database events sender',
    long_description='Tachyon DBSender\n'\
        'Part of Tachyon event management console.\n'\
        'Send events from SQL database into Tachyon.',
    #url='',
    author="Michal Belica",
    author_email="devel@beli.sk",
    license="GPL-3",
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2.7',
        # TODO 'Programming Language :: Python :: 3',
        'Topic :: Utilities',
        ],
    keywords=['tachyon', 'monitoring'],
    zip_safe=True,
    install_requires=[
        'python-daemon>=1.5',
        'amqp',
        ],
    extras_require={
        'postgresql': ['psycopg2'],
        'oracle': ['cx_oracle'],
        },
    package_dir={'': 'src'},
    packages=['tachyon_dbsender'],
    entry_points={
        'console_scripts': [
            'tachyon_dbs = tachyon_dbsender.core:main',
            ],
        },
    )

