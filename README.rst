Tachyon DBsender
================

Send events from database into Tachyon.

For more information please visit `Tachyon homepage`_.

.. _Tachyon homepage: https://tachyon.link

License
-------

Copyright 2014-2015 Michal Belica <devel@beli.sk>

::
                                                                                         
    Tachyon DBSender is free software: you can redistribute it and/or modify                     
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    Tachyon DBSender is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with Tachyon DBSender.  If not, see http://www.gnu.org/licenses/ .

A copy of the license can be found in the ``LICENSE`` file in the
distribution.

