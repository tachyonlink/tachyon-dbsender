Tachyon DBSender Change Log
===========================

All notable changes to this project will be documented in this file.

This project adheres to `Semantic Versioning`_.

Format of this change log follows suggestions on `Keep a CHANGELOG`_ with the notable
exception that it uses reStructuredText_ instead of Markdown.

.. _Semantic versioning: http://semver.org/
.. _Keep a CHANGELOG: http://keepachangelog.com/
.. _reStructuredText: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html


[0.2.1] - 2015-06-16
~~~~~~~~~~~~~~~~~~~~

Hotfix release.

Fixed
-----
- Problem with manual poll falsely detecting new events.

[0.2.0] - 2015-06-11
~~~~~~~~~~~~~~~~~~~~

First public release.

Fixed
-----
- Logging and daemonize fixes
- Handle empty tables
- Handle empty state files

Changed
-------
- Migrated to AMQP 0-9-1, used with RabbitMQ
- Extra dependencies in Python packaging

Added
-----
- RPM packaging using setuptools bdist_rpm
- Debug command line option (no detach, log to console)

[0.1.2] - 2014-12-03
~~~~~~~~~~~~~~~~~~~~
Fixed
-----
- Oracle tweaks
- Debian initscript fix
- Debian dependencies

[0.1.1] - 2014-07-28
~~~~~~~~~~~~~~~~~~~~
Fixed
-----
- PID file permissions
- Logging improvements
- Python distribution improvements

Changed
-------
- Moved to Qpid Proton Messenger API AMQP 1.0

Added
-----
- Command line args
- Debian packaging & init script

[0.1.0] - 2014-07-12
~~~~~~~~~~~~~~~~~~~~
Added
-----
- Python distribution

