# Tachyon DB Sender
# Copyright (C) 2014,2015 Michal Belica <devel@beli.sk>
#
# This file is part of Tachyon DB Sender.
#
# Tachyon DB Sender is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tachyon DB Sender is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tachyon DB Sender.  If not, see <http://www.gnu.org/licenses/>.

import logging
_logger = logging.getLogger(__name__)

from messaging import Messaging as ExMessaging

class MessagingError(Exception): pass
class SendingError(MessagingError): pass
class MessageAborted(SendingError): pass
class MessageRejected(SendingError): pass

class Messaging(object):
    def open(self, address=None, timeout=10):
        self.address = address
        self.messaging = ExMessaging()
        self.messaging.connect()
        self.messaging.default_publish(exchange='', routing_key=address)
        return self

    def close(self):
        if hasattr(self, 'messaging'):
            self.messaging.close()
            del self.messaging

    def _ensure_open(self):
        if not hasattr(self, 'messaging'):
            self.open()

    def ping(self):
        self.send_str('')
        return self

    def send_str(self, data):
        """Send message with contents from string ``data``.

        If no address is given, use address from previous :meth:open call.

        :raises: MessageAborted, MessageRejected, SendingError
        """
        self._ensure_open()
        self.messaging.send(data)

