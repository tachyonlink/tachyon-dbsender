# Tachyon DB Sender
# Copyright (C) 2014,2015 Michal Belica <devel@beli.sk>
#
# This file is part of Tachyon DB Sender.
#
# Tachyon DB Sender is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tachyon DB Sender is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tachyon DB Sender.  If not, see <http://www.gnu.org/licenses/>.

import json
import errno

class PDict(dict):
    def __init__(self, filename, *a, **kw):
        self.filename = filename
        super(PDict, self).__init__(*a, **kw)

    def save(self):
        with open(self.filename, 'w') as f:
            json.dump(self, f)
        return self

    def load(self):
        self.clear()
        try:
            with open(self.filename, 'r') as f:
                d = json.load(f)
                self.update(d)
        except IOError as e:
            if e.errno != errno.ENOENT:
                raise
        except:
            raise
        return self

