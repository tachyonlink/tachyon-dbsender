# Tachyon DB Sender
# Copyright (C) 2014,2015 Michal Belica <devel@beli.sk>
#
# This file is part of Tachyon DB Sender.
#
# Tachyon DB Sender is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tachyon DB Sender is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tachyon DB Sender.  If not, see <http://www.gnu.org/licenses/>.

import sys
import time
import json
import argparse
import threading

import daemon
try:
    from daemon.pidlockfile import TimeoutPIDLockFile
except ImportError:
    from daemon.pidfile import TimeoutPIDLockFile

from tachyon_dbsender.dbtail import DBTail, DBTailError
from tachyon_dbsender.pdict import PDict
from tachyon_dbsender.config import parse_config
from tachyon_dbsender.logsetup import LogSetup
from tachyon_dbsender.msg import Messaging
from tachyon_dbsender.utils import DTEncoder, get_uid_gid, dummy_ctx
from tachyon_dbsender import __version__, __description__

import logging
_logger = logging.getLogger(__name__)


class TDBSenderThread(threading.Thread):
    def __init__(self, name, config, address, shutdown_event):
        super(TDBSenderThread, self).__init__()
        self.name = name
        self.config = config
        self.address = address
        self.shutdown_event = shutdown_event

    def _main_loop(self):
        while not self.shutdown_event.is_set():
            if self.dbtail.poll(self.config['poll_interval']):
                starttime = time.time()
    
                try:
                    count = 0
                    for row in self.dbtail:
                        self.messaging.send_str(json.dumps([self.config['source'], [row]], cls=DTEncoder))
                        self.state['last_event'] = self.dbtail.last_event
                        count += 1
                finally:
                    _logger.info('Sent %d events', count)
                    self.state.save()
    
                endtime = time.time()
                _logger.debug('Processing took %.1g s', endtime - starttime)
                time.sleep(2)
    
    
    def run(self):
        # msg init
        self.messaging = Messaging().open(self.address).ping()
                
        self.state = None
        try:
            self.state = PDict(self.config['state_filename']).load().save() # just to be sure we can write
            with DBTail(
                    self.config['sql_driver'],
                    self.config['dsn'],
                    self.config['sql_new_events'],
                    self.config['sql_last_event'],
                    self.config['sql_event_key'],
                    self.config.get('sql_notify_channel'),
                    self.state.get('last_event'),
                    ) as self.dbtail:
                self._main_loop()
                _logger.info('Thread %s finished.', self.name)
        except:
            _logger.critical('Thread %s failed.', self.name, exc_info=True)
        finally:
            if self.state:
                self.state.save()


class TachyonDBSender(object):
    """Application class"""

    def start(self):
        """Application class entry point"""
    
        # early logging config, before we read user supplied config
        logging.basicConfig(level=logging.DEBUG)
        
        self._program_init()
    
        self.shutdown_event = threading.Event()
        uid, gid = get_uid_gid(self.gconfig.get('user'), self.gconfig.get('group'))
        try:
            if not self.args.debug:
                daemon_ctx = daemon.DaemonContext(
                        working_directory = '/',
                        umask = 0o022,
                        uid = uid,
                        gid = gid,
                        pidfile = TimeoutPIDLockFile(self.gconfig['pid_file'], 2),
                        files_preserve = self.logsetup.get_fds(),
                        )
                # do not close stdout if logging to console
                if self.logsetup.use_stderr:
                    daemon_ctx.stderr = sys.stderr
            else:
                daemon_ctx = dummy_ctx()
            with daemon_ctx:
                self._spawn_threads()
        except (SystemExit, KeyboardInterrupt) as e:
            _logger.info('%s: %s', e.__class__.__name__, e)
        except:
            _logger.critical('Tachyon DB Sender failed.', exc_info=True)
        finally:
            _logger.info('Tachyon DB Sender finished.')
    
    def _program_init(self):
        """Initialization called at program start.
        - handle command line arguments
        - read config files
        - setup logging
        """
        # command line arguments
        parser = argparse.ArgumentParser(description=__description__)
        group = parser.add_mutually_exclusive_group(required=True)
        group.add_argument('--config', '-c', metavar='CONFIGFILE', help='config file name')
        group.add_argument('--version', '-V', action='version', help='show version and exit',
                version='tachyon-dbsender {}\n'.format(__version__))
        parser.add_argument('--debug', '-d', action='store_true', help='debug mode (no detach)')
        self.args = parser.parse_args()
    
        # read config file
        config_values = {
                'address': str,
                'source': str,
                'dsn': str,
                'sql_driver': str,
                'sql_new_events': str,
                'sql_last_event': str,
                'sql_event_key': str,
                'sql_notify_channel': str,
                'poll_interval': int,
                'state_filename': str,
                'pid_file': str,
                'logging': str,
                'user': str,
                'group': str,
                }
        self.config = parse_config(self.args.config, config_values)
        # separate global section from event collectors
        self.gconfig = self.config['TACHYON_DBSENDER']
        del(self.config['TACHYON_DBSENDER'])
        
        self.logsetup = LogSetup(config=self.gconfig['logging'],
                app_name='tachyon-dbsender').configure()

    def _spawn_threads(self):
        self.main_thread = threading.current_thread()
        self.threads = []
        try:
            for name, config_section in self.config.iteritems():
                _logger.info('Starting thread %s', name)
                thread = TDBSenderThread(
                        name, config_section, self.gconfig['address'],
                        self.shutdown_event)
                thread.start()
                self.threads.append(thread)
            # check if a thread dies
            while True:
                for t in self.threads:
                    if not t.is_alive():
                        _logger.error('Thread %s died', t.name)
                        break
                else:
                    time.sleep(1)
                    continue
                break
        finally:
            # try to tear down (remaining) threads
            _logger.info('Stopping threads')
            self.shutdown_event.set()
            for t in self.threads:
                _logger.debug('Joining %s', t.name)
                t.join()

def main():
    """Module entry point"""
    TachyonDBSender().start()

