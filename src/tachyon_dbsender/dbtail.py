# Tachyon DB Sender
# Copyright (C) 2014,2015 Michal Belica <devel@beli.sk>
#
# This file is part of Tachyon DB Sender.
#
# Tachyon DB Sender is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tachyon DB Sender is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tachyon DB Sender.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import re
import time
import select
import logging
_logger = logging.getLogger(__name__)

class DBTailError(Exception):
    pass

class NotFound(Exception):
    pass

class DBTail(object):
    db_name_valid = r'^([\w-]+)$'

    def __init__(self, driver, dsn, sql_new_events, sql_last_event, last_event_key,
            listen_channel=None, last_event=None):
        self.driver = str(driver).lower()
        self.dsn = dsn
        self.sql_new_events = sql_new_events
        self.sql_last_event = sql_last_event
        self.last_event_key = last_event_key
        try:
            self.listen_channel = re.match(self.db_name_valid, listen_channel).group(1) if listen_channel else None
        except AttributeError:
            raise DBTailError('listen_channel is an invalid database identifier')
        self.conn = None
        self.cursor = None
        self.backend_pid = None
        _logger.debug('setting last event to %s', repr(last_event))
        self.last_event = last_event
        self.last_event_idx = None
        self.rows = []
        self.unread = False
        self.cols = None
        self.first_poll = True

    def poll(self, timeout=0):
        if not self.first_poll and self.listen_channel:
            # use notifications
            _logger.debug('poll(): waiting for notification')
            if select.select([self.conn],[],[self.conn],timeout) == ([],[],[]):
                return False
            else:
                self.conn.poll()
                notified = False
                for notify in self.conn.notifies:
                    if notify.channel == self.listen_channel:
                        _logger.debug('poll(): notify')
                        notified = True
                        break
                del(self.conn.notifies[:])
                return notified
        else:
            # poll DB (always do a manual poll the first time)
            if self.first_poll:
                self.first_poll = False
            _logger.debug('poll(): manual poll')
            try:
                db_last_event = self._get_db_value(self.sql_last_event)
            except NotFound:
                db_last_event = 0
            if self.last_event is None or self.last_event < db_last_event:
                # new events
                _logger.error('New events')
                return True
            elif self.last_event > db_last_event:
                _logger.error('Database log truncated! Manual recovery needed. saved_id=%d last_id=%d', self.last_event, db_last_event)
                if timeout:
                    time.sleep(timeout)
            elif timeout:
                time.sleep(timeout)
            return False

    ### compound (with) interfce
    def __enter__(self):
        return self.prepare()

    def __exit__(self, type, value, traceback):
        self.close()
    ### end compound (with) interfce

    ### iterator interface
    def __iter__(self):
        return self

    def next(self):
        if self.rows:
            _logger.debug('next(): cached row')
            return dict(zip(self.cols, self._update_last(self.rows.pop(0))))
        if not self.unread:
            _logger.debug('next(): execute query: %s (param: %s)', self.sql_new_events, str(self.last_event))
            self.cursor.execute(self.sql_new_events, (0 if self.last_event is None else self.last_event,))
            self.unread = True
            if self.cols is None:
                self._set_cols()
        if self.unread:
            rows = self.cursor.fetchmany()
            _logger.debug('next(): fetch %d', len(rows))
            if rows:
                self.rows.extend(rows)
                return dict(zip(self.cols, self._update_last(self.rows.pop(0))))
            else:
                _logger.debug('next(): finish')
                self.unread = False
                raise StopIteration
    ### end iterator interface

    def prepare(self):
        _logger.debug('prepare()')
        self._connect()
        if self.listen_channel:
            self.cursor.execute('LISTEN "{0}";'.format(self.listen_channel))
        if self.last_event is None:
            try:
                self.last_event = self._get_db_value(self.sql_last_event)
            except NotFound:
                self.last_event = 0
        return self

    def close(self):
        if self.cursor:
            self.cursor.close()
            self.cursor = None
        if self.conn:
            self.conn.close()
            self.conn = None

    def _update_last(self, row):
        rowkey = row[self.last_event_idx]
        if rowkey > self.last_event:
            self.last_event = rowkey
        return row

    def _set_cols(self):
        self.cols = []
        for coldesc in self.cursor.description:
            self.cols.append(coldesc[0].lower())
        self.last_event_idx = self.cols.index(self.last_event_key)

    def reconnect(self):
        self.close()
        self.prepare()
        return self

    def _connect(self):
        if self.driver == 'postgresql':
            import psycopg2
            self.conn = psycopg2.connect(self.dsn)
            self.conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            self.cursor = self.conn.cursor()
            self.cursor.arraysize = 10
            self.backend_pid = self._get_db_value("SELECT pg_backend_pid();")
        elif self.driver == 'oracle':
            import cx_Oracle
            self.conn = cx_Oracle.connect(self.dsn)
            self.cursor = self.conn.cursor()
            self.cursor.arraysize = 10
            _logger.debug('Oracle DB connected.')
        else:
            raise DBTailError('Unknown DB driver "{0}"'.format(self.driver))

    def _get_db_value(self, stmt):
        self.cursor.execute(stmt)
        row = self.cursor.fetchone()
        if not row:
            raise NotFound
        else:
            return row[0]

