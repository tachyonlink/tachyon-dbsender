# Tachyon DB Sender
# Copyright (C) 2014,2015 Michal Belica <devel@beli.sk>
#
# This file is part of Tachyon DB Sender.
#
# Tachyon DB Sender is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tachyon DB Sender is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tachyon DB Sender.  If not, see <http://www.gnu.org/licenses/>.

import logging

import amqp

_logger = logging.getLogger(__name__)

class Messaging(object):
    def __init__(self, *args, **kwargs):
        self.messages = []
        if not hasattr(self, 'logger'):
            self.logger = _logger

    def connect(self):
        self.connection = amqp.Connection()
        self.channel = self.connection.channel()
        self.channel.basic_qos(prefetch_size=0, prefetch_count=1, a_global=False)
        return self

    def consume(self, **kwargs):
        self.channel.basic_consume(callback=self.callback, **kwargs)

    def callback(self, msg):
        self.messages.append(msg)

    def ack(self, delivery_tag=None, msg=None):
        if delivery_tag is not None:
            self.channel.basic_ack(delivery_tag=delivery_tag)
        elif msg is not None:
            msg.channel.basic_ack(delivery_tag=msg.delivery_tag)
        else:
            return Exception('ack() takes one of msg or delivery_tag parameters.')

    def reject(self, delivery_tag=None, msg=None, requeue=False):
        if delivery_tag is not None:
            self.channel.basic_reject(delivery_tag=delivery_tag, requeue=requeue)
        elif msg is not None:
            msg.channel.basic_reject(delivery_tag=msg.delivery_tag, requeue=requeue)
        else:
            return Exception('reject() takes one of msg or delivery_tag parameters.')

    def default_publish(self, **kwargs):
        self.default_pub_params = kwargs

    def send(self, body, **kwargs):
        msg = amqp.Message(body=body, channel=self.channel, properties={'delivery_mode': 2})
        if kwargs:
            self.channel.basic_publish(msg=msg, **kwargs)
        else:
            self.channel.basic_publish(msg=msg, **self.default_pub_params)
        return msg

    def receive(self):
        self.channel.wait()
        ret = self.messages
        self.messages = []
        return ret

    def close(self):
        if hasattr(self, 'connection'):
            self.connection.close()

    def declare(self, declares):
        for obj, params in declares:
            if obj == 'queue':
                self.logger.debug('declaring %s %r' % (obj, params))
                self.channel.queue_declare(**params)
            elif obj == 'exchange':
                self.logger.debug('declaring %s %r' % (obj, params))
                self.channel.exchange_declare(**params)
            elif obj == 'queue_bind':
                self.logger.debug('declaring %s %r' % (obj, params))
                self.channel.queue_bind(**params)
            elif obj == 'exchange_bind':
                self.logger.debug('declaring %s %r' % (obj, params))
                self.channel.exchange_bind(**params)
            else:
                raise Exception('Declaration of unknown object "{}"'.format(obj))
        return self

    def __enter__(self):
        return self.connect()

    def __exit__(self, *args, **kwargs):
        self.close()
