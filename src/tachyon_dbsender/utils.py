# Tachyon DB Sender
# Copyright (C) 2014,2015 Michal Belica <devel@beli.sk>
#
# This file is part of Tachyon DB Sender.
#
# Tachyon DB Sender is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tachyon DB Sender is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tachyon DB Sender.  If not, see <http://www.gnu.org/licenses/>.

import os
import pwd
import grp
import time
import json
import datetime
import contextlib

class DTEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return int(time.mktime(obj.timetuple()))
        return super(DTEncoder, self).default(obj)

def get_uid_gid(username, groupname=None):
    if username:
        pw_ent = pwd.getpwnam(username)
        uid = pw_ent[2]
        ugid = pw_ent[3]
    else:
        uid = os.getuid()
        ugid = os.getgid()
    gid = grp.getgrnam(groupname)[2] if groupname else ugid
    return (uid, gid)

@contextlib.contextmanager
def dummy_ctx():
    yield

