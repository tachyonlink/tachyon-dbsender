# Tachyon DB Sender
# Copyright (C) 2014,2015 Michal Belica <devel@beli.sk>
#
# This file is part of Tachyon DB Sender.
#
# Tachyon DB Sender is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tachyon DB Sender is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tachyon DB Sender.  If not, see <http://www.gnu.org/licenses/>.

"""Reading config files"""

import ConfigParser

def parse_config(filename, config_values):
    """Read config filename into a dictionary, coercing values according to config_values"""

    configfile = ConfigParser.SafeConfigParser()
    with open(filename, 'r') as f:
        configfile.readfp(f, filename)

    config = {}
    for section in configfile.sections():
        config[section] = {}
        for option in configfile.options(section):
            try:
                option_type = config_values[option]
            except KeyError:
                option_type = str
            config[section][option] = option_type(configfile.get(section, option))
    return config
