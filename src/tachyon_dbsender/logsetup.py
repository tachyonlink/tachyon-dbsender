# Tachyon DB Sender
# Copyright (C) 2014,2015 Michal Belica <devel@beli.sk>
#
# This file is part of Tachyon DB Sender.
#
# Tachyon DB Sender is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tachyon DB Sender is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tachyon DB Sender.  If not, see <http://www.gnu.org/licenses/>.

import syslog
import logging
import logging.handlers

def _facility(fac_str):
    return getattr(syslog, 'LOG_{}'.format(fac_str.upper()))

def _unpack(seq, num, default=None):
    """Unpacks 'items' number of items from a sequence,
    filling with 'default' if sequence is shorter or cutting off excessive items.
    Returns a list."""
    ret = []
    for i in range(num):
        try:
            ret.append(seq[i])
        except IndexError:
            ret.append(default)
    return ret

class ContextFilter(logging.Filter):
    """Add attributes from context mapping to record objects."""
    def __init__(self, context):
        self.context = context

    def filter(self, record):
        for k, v in self.context.iteritems():
            setattr(record, k, v)
        return True


class LogSetup(object):
    syslog_fmt='%(appName)s[%(process)s]: (%(name)s) %(message)s'
    default_fmt='%(asctime)s [%(levelname)s] %(name)s: %(message)s'

    def __init__(self, fmt=None, datefmt='%F %T', config=None, app_name=None):
        self.fmt = fmt
        self.datefmt = datefmt
        self.config = config
        self.configured = False
        self.handler = None
        self.context_filter = ContextFilter({'appName': app_name}) if app_name else None
        self.use_stderr = False

    def set_handler(self):
        oldhandler = self.handler
        if self.config:
            if self.config.startswith('syslog:') or self.config == 'syslog':
                hander, fac, addr = _unpack(self.config.split(':'), 3)
                self.handler = logging.handlers.SysLogHandler(
                        address=addr if addr else '/dev/log',
                        facility=_facility(fac if fac else 'daemon'),
                        ) # FIXME default address not very portable
                fmt = self.syslog_fmt
            elif self.config.startswith('file:'):
                hander, filename = _unpack(self.config.split(':'), 2)
                self.handler = logging.FileHandler(filename=filename)
                fmt = self.default_fmt
            else:
                raise ValueError('logging config has to start with "syslog" or "file": %s' % self.config)
        else:
            self.handler = logging.StreamHandler()
            self.use_stderr = True
        self.formatter = logging.Formatter(fmt=self.fmt if self.fmt else self.default_fmt, datefmt=self.datefmt)
        self.handler.setFormatter(self.formatter)
        if self.context_filter:
            self.handler.addFilter(self.context_filter)
        self.root_logger.addHandler(self.handler)
        if oldhandler:
            root_logger.removeHandler(oldhandler)
        return self

    def remove_handlers(self, logger):
        for handler in logger.handlers:
            logger.removeHandler(handler)

    def configure(self):
        # root logger
        self.root_logger = logging.getLogger('')
        self.root_logger.setLevel(logging.DEBUG)
        # log handler
        if not self.configured:
            # remove handlers set before
            self.remove_handlers(self.root_logger)
        self.set_handler()
        # special log levels
        logging.getLogger('qpid').setLevel(logging.INFO)
        self.configured = True
        return self

    def get_fds(self):
        if self.handler:
            if hasattr(self.handler, 'sock'):
                # socket types
                return [self.handler.sock]
            elif hasattr(self.handler, 'socket'):
                # e.g. SysLogHandler
                return [self.handler.socket]
            elif hasattr(self.handler, 'stream'):
                # for file handlers
                return [self.handler.stream]
            else:
                raise Exception('Handler is neither file nor socket type.')
        else:
            # no handlers configured
            return []

